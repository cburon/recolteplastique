# Récolte de plastiques en mer
Bienvenue dans le projet de récolte de plastiques en mer. Ce projet est destiné à des étudiant.e.s de niveau M1 de l'ISEN. Il a pour objectif d'implémenter un algorithme d'enchères collaboratives type CBAA ou CBBA [[1](#CBBA)].

## Présentation générale
La problématique de la récupération des plastiques marins a pris une importance conséquente dans les questions ayant trait à la pollution marine. Les plastiques se désagrègent, se transformant en micro-plastiques plus difficiles à récolter et polluant la mer à grande échelle, ou encore intoxiquent la faune marine qui les ingère. Les drones de surface autonomes (USV) donnent la possibilité de récolter ces plastiques sans nécessiter d’équipage humain. Une autonomisation plus importante de ces drones permettrait de réaliser les récoltes à une plus grande échelle, mais cette autonomisation passe par une coordination autonome et donc nécessairement décentralisée de ces drones. Ce projet aura pour ambition d’implémenter un algorithme de coordination de drones de surface basé sur les mécanismes d’enchères permettant de planifier de manière décentralisée la récolte de ces plastiques et de s’adapter à leur découverte.

## Implémentation

Pour ce faire, il s’appuiera sur le domaine des systèmes multi-agents. Il consistera à implémenter un algorithme d’enchères sur un ensemble de plastiques découverts, permettant ainsi de déterminer un plan commun de récolte des déchets parmi les agents. Le projet sera développé en python à l'aide d'un framework spécifique agent, Mesa, et sera évalué à travers une série d'expérimentations sous forme de simulations. Pour la définition des actes de langages associés aux messages, une autre bibliothèque -- PADE -- sera utilisée.

## Installation et lancement

Pour installer les dépendances, vous pouvez utiliser le fichier `requirements.txt`:
```
pip install -r requirements.txt
```

Pour lancer le projet, il vous suffit de lancer avec python le fichier `plastic.py`:
```
python plastic.py
```

Cette ligne de code devrait ouvrir un onglet dans votre navigateur avec la visualisation du projet.

Si ce n'est pas le cas, vous pouvez cliquer [ici](127.0.0.1:8521/#)


<a name="CBBA"></a>[1] Choi, H. L., Brunet, L., & How, J. P. (2009). Consensus-based decentralized auctions for robust task allocation. *IEEE transactions on robotics*, 25(4), 912-926.
