#imports standards
import math
import random
import string
import numpy as np
from collections import defaultdict

#imports de mesa


import mesa
import mesa.space
from mesa import Agent, Model, DataCollector
from mesa.time import RandomActivation
from mesa.visualization import ModularVisualization
from mesa.visualization.ModularVisualization import VisualizationElement, ModularServer
from mesa.visualization.modules import ChartModule

#import de PADE pour les messages
from pade.acl.messages import ACLMessage  # Vous trouverez dans cette classe tous les performatifs

mailboxes = {}  # Les boites aux lettres des agents qui leurs permettent de gérer les messages. Variable globale

class Message:
    def __init__(self, act: string, content, sender, receivers):
        self.act = act  # act doit etre un acte de langage pris dans ACLMessage
        self.content = content  # contenu. Pour le moment, peut etre n'importe quel type
        self.sender = sender  # l'agent ayant envoyé le message
        self.receivers = receivers  # liste des agents destinataires. Doit etre sous la forme d'une liste,
                                    # meme d'un unique agent

    def send(self):
        """
        Méthode d'envoi du message
        :return: None
        """
        for a in self.receivers:
            mailboxes[a].append(self)


class RecuperationPlastique(mesa.Model):
    def __init__(self, n_dechets, n_usv):
        mesa.Model.__init__(self)
        self.space = mesa.space.ContinuousSpace(600, 600, False)  # Espace utilisé (600*600px)
        # scheduler, active les agents. Ici, le random scheduler les active de manière aléatoire à chaque step
        self.schedule = RandomActivation(self)
        self.usvs = []  # Liste des USVs
        self.dechets = []  # Liste des déchets à tenir à jour. Utilisé pour la courbe en bas de l'écran
        for i in range(n_dechets):  # Creation des déchets
            dechet = Dechet(i, self, random.randint(0, 600), random.randint(0, 600))
            self.dechets.append(dechet)
            self.schedule.add(dechet)  # ajouté au scheduler
        for i in range(n_usv):  # création des USV
            bateau = USV(i + n_dechets, self, random.randint(0, 600), random.randint(0, 600), 15)
            self.usvs.append(bateau)
            self.schedule.add(bateau)

            # FIXME L'USV i est alloué au déchet i s'il existe. A modifier avec votre algorithme.
            if i < len(self.dechets):
                bateau.goto(self.dechets[i])

            # Création du datacollector. Collecte des données pour la courbe en bas de l'écran.
            # Servira aussi pour la collecte de données en batch
            self.datacollector = DataCollector(
                model_reporters={"dechets": lambda model: len(model.dechets),
                                 # "Delivered": lambda model: model.computed_items_nb
                                 },
                agent_reporters={})

    def step(self):  # Fonction activée par le modèle à chaque tour
        self.schedule.step()  # On active les agents à travers le scheduler qui appelle la méthode step des agents
        self.datacollector.collect(self)  # On recalcule les données
        if self.schedule.steps >= 300 or len(self.dechets) == 0:
            self.running = False  # S'il n'y a plus de déchets ou si le nombre de tours est supérieur à 300, on s'arrête


class CommunicatingAgent(Agent):  # classe héritée d'Agent qui gère les messages
    # (si jamais on voulait ajouter autre chose que des USV par exemple)
    def __init__(self, unique_id: int, model: Model):
        super().__init__(unique_id, model)
        mailboxes[self] = []  # on crée une boite aux lettres pour l'agent

    def send(self, msg: Message):
        """
        Fonction d'envoi d'un message. Appelle simplement la méthode d'envoi du message
        :param Message msg: le message à envoyer
        :return: None
        """
        msg.send()

    def receive(self):
        """
        Lors de la réception, on renvoie la liste des messages et on la vide
        :return: la liste des messages reçus
        """
        messages = mailboxes[self]
        mailboxes[self] = []
        return messages


class USV(CommunicatingAgent):  # L'agent USV
    def __init__(self, unique_id: int, model: RecuperationPlastique,
                 x, y, max_speed: float):
        super().__init__(unique_id, model)
        self.x = x  # position en x
        self.y = y # position en y
        self.max_speed = max_speed  # vitesse max pour le déplacement
        self.destination = None  # destination (pour l'utilisation de move)

    def goto(self, destination):
        """
        Établit la destination de l'agent
        :param destination: doit avoir un x et un y, la destination de l'agent
        :return: None
        """
        self.destination = destination

    def move(self):
        """
        Se déplace à vitesse maximale vers la destination; appelle move_to
        :return:
        """
        self.move_to(self.destination, self.max_speed)

    def move_to(self, dest, speed):
        """
        Se déplace à une vitesse déterminée vers une destination déterminée
        :param dest: destination. Doit avoir un x et un y
        :param float speed: vitesse
        :return: None
        """
        movement = tuple(min(
            (speed * (dest.x - self.x) / np.linalg.norm((dest.x - self.x, dest.y - self.y)), speed * (dest.y - self.y) /
             np.linalg.norm((dest.x - self.x, dest.y - self.y))),
            (dest.x - self.x, dest.y - self.y), key=lambda p: np.linalg.norm(p)))
        self.x += movement[0]
        self.y += movement[1]

    def step(self):  # FIXME Il faut complètement changer cette méthode et y mettre celle qui correspond à CBAA/CBBA
        if self.destination:  # déplacement vers la destination s'il y en a une
            if (self.x, self.y) != (self.destination.x, self.destination.y):
                self.move()
            else: # s'il est à sa destination, supprime le plastique en reçoit puisque tous les agents le lui envoient
                self.model.schedule.remove(self.destination)
                self.model.dechets.remove(self.destination)
                self.destination = None  # supprime sa destination

                # exemple d'envoi d'un message à l'USV 0
                self.send(Message(ACLMessage.INFORM, "Mission accomplie", self, [self.model.usvs[0]]))
        messages = self.receive()  # réception. Seul l'agent 0
        for m in messages:
            print("USV " + str(m.sender.unique_id) + ' dit "' + m.content + '"')  # affiche les messages reçus

    def portrayal_method(self):
        """
        Méthode d'affichage utilisé par le js
        :return: l'objet à interpréter par le javascript
        """
        portrayal = {"Shape": "arrowHead", "s": 1, "Filled": "true", "Color": "DimGrey", "Layer": 2, 'x': self.x,
                     'y': self.y}
        if self.destination and not (self.destination.x == self.x and self.destination.y == self.y):
            if self.destination.y - self.y > 0:
                portrayal['angle'] = math.acos((self.destination.x - self.x) /
                                                   np.linalg.norm(
                                                       (self.destination.x - self.x, self.destination.y - self.y)))
            else:
                portrayal['angle'] = 2 * math.pi - math.acos((self.destination.x - self.x) /
                                                                 np.linalg.norm(
                                                                     (self.destination.x - self.x,
                                                                      self.destination.y - self.y)))
        else:
            portrayal['angle'] = 0
        return portrayal


class Dechet(Agent):
    def __init__(self, unique_id: int, model, x, y):
        super().__init__(unique_id, model)
        self.x = x  # position en x
        self.y = y  # position en y

    def step(self):  # Pour le moment, les déchets ne bougent pas
        pass

    @staticmethod
    def portrayal_method():
        color = "GreenYellow"
        r = 6
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Layer": 1,
                     "Color": color,
                     "r": r}
        return portrayal


class ContinuousCanvas(VisualizationElement):
    #  Les includes utilisés pour l'affichage dans le navigateur
    local_includes = [
        "./js/simple_continuous_canvas.js",  # le js pour l'affichage principal
        "./js/jquery.min.js",  # jquery, également nécessaire pour l'affichage
    ]

    def __init__(self, canvas_height=500,
                 canvas_width=500, instantiate=True):
        # Visualisation
        VisualizationElement.__init__(self)
        self.canvas_height = canvas_height
        self.canvas_width = canvas_width
        self.identifier = "sea-canvas"
        if instantiate:
            new_element = ("new Simple_Continuous_Module({}, {},'{}')".
                           format(self.canvas_width, self.canvas_height, self.identifier))
            self.js_code = "elements.push(" + new_element + ");"

    @staticmethod
    def portrayal_method(obj):
        """
        Pour afficher un élément, on appelle sa fonction portrayal_method
        :param obj: l'objet à afficher
        :return: None
        """
        return obj.portrayal_method()

    def render(self, model):
        """
        Rendu du model lui-meme
        :param model: le PlasticModel
        :return: l'objet à interpréter par javascript
        """
        representation = defaultdict(list)
        for obj in model.schedule.agents:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
                representation[portrayal["Layer"]].append(portrayal)
        return representation


def run_single_server():
    """
    Fonction principale d'appel. C'est ici qu'on définira si on affiche ou si on fait un batch
    :return:
    """
    chart = ChartModule([{"Label": "dechets",  # la courbe
                          "Color": "Red"},
                         # {"Label": "Delivered",
                         #  "Color": "Blue"}
                         ],
                        data_collector_name='datacollector')

    server = ModularServer(RecuperationPlastique,  # L'ensemble des éléments.
                           # Ici, on a un ModularServer avec deux éléments, la fenêtre principale et la courbe
                           [ContinuousCanvas(), chart],
                           # [ContinuousCanvas()],
                           "Recolte de dechets plastiques en mer",
                           # Les sliders qui permettent de définir le nombre de déchets/d'USV
                           {"n_dechets": ModularVisualization.UserSettableParameter('slider',
                                                                                    "Nombre de dechets",
                                                                                    10, 3, 20, 1),
                            "n_usv": ModularVisualization.UserSettableParameter('slider',
                                                                                  "Nombre d'USV",
                                                                                  15, 3, 30, 1)})
    server.port = 8521  # le port sur lequel la page est accessible (toujours 127.0.0.1:port)
    server.launch()


if __name__ == "__main__":
    run_single_server()
